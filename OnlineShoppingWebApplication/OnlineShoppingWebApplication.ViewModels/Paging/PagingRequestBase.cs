﻿namespace OnlineShoppingWebApplication.ViewModels.Paging
{
    public class PagingRequestBase
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
