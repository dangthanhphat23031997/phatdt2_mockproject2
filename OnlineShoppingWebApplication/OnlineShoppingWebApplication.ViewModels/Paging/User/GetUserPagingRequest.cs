﻿namespace OnlineShoppingWebApplication.ViewModels.Paging.User
{
    public class GetUserPagingRequest : PagingRequestBase
    {
        public string Keyword { get; set; }
    }
}
