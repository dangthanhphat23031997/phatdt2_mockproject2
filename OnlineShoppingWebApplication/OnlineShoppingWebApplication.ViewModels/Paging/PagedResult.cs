﻿using System.Collections.Generic;

namespace OnlineShoppingWebApplication.ViewModels.Paging
{
    public class PagedResult<T> : PagedResultBase
    {
        public List<T> Items { get; set; }
    }
}
