﻿using Microsoft.EntityFrameworkCore;
using OnlineShoppingWebApplication.Core.Data.Extensions;
using System.Reflection;

namespace OnlineShoppingWebApplication.Core.Data
{
    public class OnlineShoppingWebApplicationDbContext : DbContext
    {
        public OnlineShoppingWebApplicationDbContext()
        {

        }

        public OnlineShoppingWebApplicationDbContext(DbContextOptions<OnlineShoppingWebApplicationDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            builder.Seed();
        }
    }
}
