﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineShoppingWebApplication.Core.Entities;

namespace OnlineShoppingWebApplication.Core.Data.Configurations
{
    public class BaseRoleConfiguration : IEntityTypeConfiguration<BaseRole>
    {
        public void Configure(EntityTypeBuilder<BaseRole> builder)
        {
            builder.ToTable("Role");

            builder.Property(x => x.Description)
                .HasMaxLength(200).IsRequired(false);
        }
    }
}
