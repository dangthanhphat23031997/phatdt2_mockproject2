﻿using System;

namespace OnlineShoppingWebApplication.Core.Entities
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
