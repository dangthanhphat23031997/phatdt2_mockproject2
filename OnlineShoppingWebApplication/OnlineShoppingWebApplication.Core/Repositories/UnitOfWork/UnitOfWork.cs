﻿using OnlineShoppingWebApplication.Core.Data;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly OnlineShoppingWebApplicationDbContext _context;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IProductRepository _productRepository;

        public UnitOfWork(OnlineShoppingWebApplicationDbContext context)
        {
            _context = context;
        }

        public ICategoryRepository CategoryRepository => _categoryRepository ?? new CategoryRepository(_context);

        public IProductRepository ProductRepository => _productRepository ?? new ProductRepository(_context);        
        
        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        public async Task DisposeAsync()
        {
            if(_context != null)
            {
                await _context.DisposeAsync();
            }
        }

        public bool SaveChanges()
        {
            var result = _context.SaveChanges();
            return result > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            var result = await _context.SaveChangesAsync();
            return result > 0;
        }
    }
}
