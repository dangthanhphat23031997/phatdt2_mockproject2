﻿using OnlineShoppingWebApplication.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories.Base
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IList<T>> GetAll();
        Task<T> GetById(Guid id);
        Task Add(T entity);
        Task AddRange(IList<T> entity);
        void Update(T entity);
        Task DeleteById(Guid id);
        void Delete(T entity);
    }
}
