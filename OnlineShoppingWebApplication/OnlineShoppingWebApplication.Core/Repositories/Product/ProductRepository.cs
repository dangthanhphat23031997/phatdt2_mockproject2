﻿using Microsoft.EntityFrameworkCore;
using OnlineShoppingWebApplication.Core.Data;
using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Core.Repositories.Base;
using OnlineShoppingWebApplication.ViewModels.Paging;
using OnlineShoppingWebApplication.ViewModels.Paging.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(OnlineShoppingWebApplicationDbContext context) : base(context)
        {

        }

        public async Task<IList<Product>> GetAllIncludeCategory()
        {
            return await _entities.Include(x => x.Category).ToListAsync();
        }

        public async Task<Product> GetByName(string name)
        {
            return await _entities.Include(x => x.Category).AsNoTracking().FirstOrDefaultAsync(x => x.Name == name);
        }

        public async Task<Product> GetIncludeCategoryById(Guid id)
        {
            return await _entities.Include(x => x.Category).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<Product>> GetListByCategoryId(Guid id)
        {
            return await _entities.Include(x => x.Category).Where(x => x.CategoryId == id).ToListAsync();
        }

        public async Task<IList<Product>> GetListByName(string name)
        {
            return await _entities.Include(x => x.Category).Where(x => x.Name.Contains(name)).ToListAsync();
        }

        public async Task<PagedResult<Product>> GetProductsPaging(GetProductPagingRequest request)
        {
            var products = _entities.AsQueryable();
            if (!string.IsNullOrWhiteSpace(request.Keyword))
            {
                products = products.Where(x => x.Name.Contains(request.Keyword));
            }

            int totalRecords = await products.CountAsync();

            var data = await products.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToListAsync();

            var result = new PagedResult<Product>()
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                TotalRecords = totalRecords,
                Items = data
            };
            return result;
        }
    }
}
