﻿using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Core.Repositories.Base;
using OnlineShoppingWebApplication.ViewModels.Paging;
using OnlineShoppingWebApplication.ViewModels.Paging.Product;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        Task<IList<Product>> GetAllIncludeCategory();
        Task<Product> GetByName(string name);
        Task<Product> GetIncludeCategoryById(Guid id);
        Task<IList<Product>> GetListByName(string name);
        Task<IList<Product>> GetListByCategoryId(Guid id);
        Task<PagedResult<Product>> GetProductsPaging(GetProductPagingRequest request);
    }
}
