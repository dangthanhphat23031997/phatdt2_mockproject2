﻿using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Core.Repositories.Base;
using OnlineShoppingWebApplication.ViewModels.Paging;
using OnlineShoppingWebApplication.ViewModels.Paging.Category;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
        Task<Category> GetByName(string name);
        Task<PagedResult<Category>> GetCategoriesPaging(GetCategoryPagingRequest request);
    }
}
