﻿using AutoMapper;
using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Service.Services.DTOs;
using OnlineShoppingWebApplication.Service.Services.User.DTOs;
using OnlineShoppingWebApplication.ViewModels.Paging;

namespace OnlineShoppingWebApplication.Service.Mappings
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Category, CategoryDto>().ReverseMap();
            CreateMap<CategoryCreateDto, Category>();
            CreateMap<PagedResult<Category>, PagedResult<CategoryDto>>(); 

            CreateMap<Product, ProductDto>().ReverseMap();
            CreateMap<ProductCreateDto, Product>();
            CreateMap<ProductImportDto, Product>();
            CreateMap<PagedResult<Product>, PagedResult<ProductDto>>();

            CreateMap<UserCreateDto, BaseUser>();
            CreateMap<UserEditDto, BaseUser>();
            CreateMap<UserDto, BaseUser>().ReverseMap();
        }
    }
}
