﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using OnlineShoppingWebApplication.Core.Data;
using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Core.Repositories.Base;
using OnlineShoppingWebApplication.Core.Repositories.UnitOfWork;
using OnlineShoppingWebApplication.Service.Services;
using OnlineShoppingWebApplication.Service.Services.User;
using System;
using OnlineShoppingWebApplication.Service.Common;
using System.Text;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace OnlineShoppingWebApplication.Service.Configurations
{
    public static class ConfigurationCoreService
    {
        public static IServiceCollection AddCoreServices(this IServiceCollection services, IConfiguration configuration)
        {
            #region Auto Mapper
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            #endregion

            #region Identity
            services.AddDefaultIdentity<BaseUser>()
                .AddRoles<BaseRole>()
                .AddEntityFrameworkStores<OnlineShoppingWebApplicationDbContext>().AddDefaultTokenProviders();
            #endregion

            #region Config Identity password
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
            });
            #endregion

            #region DI
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IUserService, UserService>();
            services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            #endregion

            #region Config Authentication
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Constants.AuthorizationConstants.ISSUER,
                        ValidAudience = Constants.AuthorizationConstants.AUDIENCE,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constants.AuthorizationConstants.KEY))
                    };
                });
            #endregion

            #region Authentication Cookie, Session
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => 
                {
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(4);
                    options.LoginPath = "/User/SignIn";
                    options.AccessDeniedPath = "/User/Forbidden";
                });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(4);
            });
            #endregion

            return services;
        }
    }
}
