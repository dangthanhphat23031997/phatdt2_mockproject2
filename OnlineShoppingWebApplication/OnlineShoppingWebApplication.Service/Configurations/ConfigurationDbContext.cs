﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnlineShoppingWebApplication.Core.Data;

namespace OnlineShoppingWebApplication.Service.Configurations
{
    public static class ConfigurationDbContext
    {
        public static IServiceCollection AddDbContexts(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<OnlineShoppingWebApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("OnlineShoppingWebDB"));
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });

            return services;
        }
    }
}
