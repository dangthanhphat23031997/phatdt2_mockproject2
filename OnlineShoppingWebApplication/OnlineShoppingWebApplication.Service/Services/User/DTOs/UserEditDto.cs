﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OnlineShoppingWebApplication.Service.Services.User.DTOs
{
    public class UserEditDto
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
