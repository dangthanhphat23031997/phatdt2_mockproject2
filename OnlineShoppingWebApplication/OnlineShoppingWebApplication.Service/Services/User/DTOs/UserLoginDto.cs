﻿using System.ComponentModel.DataAnnotations;

namespace OnlineShoppingWebApplication.Service.Services.User.DTOs
{
    public class UserLoginDto
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public bool RememberPassword { get; set; }
    }
}
