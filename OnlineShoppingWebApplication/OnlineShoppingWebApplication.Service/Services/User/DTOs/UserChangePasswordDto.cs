﻿using System.ComponentModel.DataAnnotations;

namespace OnlineShoppingWebApplication.Service.Services.User.DTOs
{
    public class UserChangePasswordDto
    {
        public string UserName { get; set; }
        [Required(ErrorMessage = "The Old Password field is required.")]
        public string OldPassword { get; set; }
        [Required(ErrorMessage = "The New Password field is required.")]
        [RegularExpression(@"^(?=.{6,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$", ErrorMessage = "Password format is invalid. EX: 123aA@")]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "The Confirm Password field is required.")]
        [Compare("NewPassword", ErrorMessage = "The New Password and Confirm Password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
