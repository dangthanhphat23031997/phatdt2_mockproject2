﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OnlineShoppingWebApplication.Service.Services.User.DTOs
{
    public class UserDto
    {
        [Required]
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        private string fullName;
        public string FullName
        {
            get { return fullName = $"{this.LastName} {this.FirstName}"; }
            set { fullName = value; }
        }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
