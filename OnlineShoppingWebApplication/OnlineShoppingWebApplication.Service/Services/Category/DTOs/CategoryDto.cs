﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OnlineShoppingWebApplication.Service.Services.DTOs
{
    public class CategoryDto
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
