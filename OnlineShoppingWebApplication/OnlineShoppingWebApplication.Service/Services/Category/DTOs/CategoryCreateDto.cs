﻿using System.ComponentModel.DataAnnotations;

namespace OnlineShoppingWebApplication.Service.Services.DTOs
{
    public class CategoryCreateDto
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
