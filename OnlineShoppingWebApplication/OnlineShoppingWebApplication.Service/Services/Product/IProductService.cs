﻿using OnlineShoppingWebApplication.Service.Common;
using OnlineShoppingWebApplication.Service.Services.DTOs;
using OnlineShoppingWebApplication.ViewModels.Paging;
using OnlineShoppingWebApplication.ViewModels.Paging.Product;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Service.Services
{
    public interface IProductService
    {
        Task<IList<ProductDto>> GetAll();
        Task<IList<ProductDto>> GetAllIncludeCategory();
        Task<ApiResult<bool>> Create(ProductCreateDto dto);
        Task<ApiResult<bool>> Edit(ProductDto dto);
        Task<ApiResult<bool>> DeleteByName(string name);
        Task<ApiResult<ProductDto>> GetByName(string name);
        Task<ApiResult<IList<ProductDto>>> GetListByName(string name);
        Task<IList<ProductDto>> GetListByCategoryId(Guid id);
        Task<ApiResult<bool>> AddRange(IList<ProductImportDto> productImports);
        Task<ApiResult<PagedResult<ProductDto>>> GetProductsPaging(GetProductPagingRequest request);
        Task<ApiResult<ProductDto>> GetById(Guid id);
        Task<ApiResult<bool>> DeleteById(Guid id);
        Task<ApiResult<ProductDto>> GetIncludeCategoryById(Guid id);
    }
}
