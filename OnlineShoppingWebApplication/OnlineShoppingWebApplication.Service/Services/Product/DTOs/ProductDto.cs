﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OnlineShoppingWebApplication.Service.Services.DTOs
{
    public class ProductDto
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public decimal? Price { get; set; }
        [Required(ErrorMessage = "The Category field is required.")]
        public Guid? CategoryId { get; set; }
        public virtual CategoryDto Category { get; set; }
    }
}
