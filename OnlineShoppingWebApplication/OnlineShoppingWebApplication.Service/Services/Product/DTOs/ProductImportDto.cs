﻿using System;

namespace OnlineShoppingWebApplication.Service.Services.DTOs
{
    public class ProductImportDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public string CategoryName { get; set; }
        public Guid CategoryId { get; set; }
        public string Log { get; set; }
    }
}
