﻿using AutoMapper;
using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Core.Repositories.UnitOfWork;
using OnlineShoppingWebApplication.Service.Common;
using OnlineShoppingWebApplication.Service.Services.DTOs;
using OnlineShoppingWebApplication.ViewModels.Paging;
using OnlineShoppingWebApplication.ViewModels.Paging.Product;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Service.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IList<ProductDto>> GetAll()
        {
            var products = await _unitOfWork.ProductRepository.GetAll();
            var productsDto = _mapper.Map<IList<ProductDto>>(products);
            return productsDto;
        }

        public async Task<IList<ProductDto>> GetAllIncludeCategory()
        {
            var products = await _unitOfWork.ProductRepository.GetAllIncludeCategory();
            var productsDto = _mapper.Map<IList<ProductDto>>(products);
            return productsDto;
        }

        public async Task<ApiResult<bool>> Create(ProductCreateDto dto)
        {
            try
            {
                var product = _mapper.Map<Product>(dto);
                await _unitOfWork.ProductRepository.Add(product);
                var result = await _unitOfWork.SaveChangesAsync();
                if (result == true)
                {
                    return new ApiSuccessResult<bool>($"Create a Product [{dto.Name}] successfully!");
                }
            }
            catch (Exception ex)
            {
                return new ApiErrorResult<bool>($"Error: {ex.Message}");
            }
            return new ApiErrorResult<bool>($"Create a Product [{dto.Name}] failed!");
        }

        public async Task<ApiResult<bool>> Edit(ProductDto dto)
        {
            try
            {
                var productEnt = await _unitOfWork.ProductRepository.GetById(dto.Id);
                if(productEnt == null)
                {
                    return new ApiErrorResult<bool>("Cannot found");
                }

                var product = _mapper.Map<ProductDto, Product>(dto, productEnt);
                _unitOfWork.ProductRepository.Update(product);
                var result = await _unitOfWork.SaveChangesAsync();
                if (result == true)
                {
                    return new ApiSuccessResult<bool>($"Edit a Product [{dto.Name}] successfully!");
                }
            }
            catch (Exception)
            {
                return new ApiErrorResult<bool>($"Edit a Product [{dto.Name}] failed!");
            }
            return new ApiErrorResult<bool>($"Edit a Product [{dto.Name}] failed!");
        }

        public async Task<ApiResult<bool>> DeleteByName(string name)
        {
            try
            {
                var product = await _unitOfWork.ProductRepository.GetByName(name);
                if (product != null)
                {
                    _unitOfWork.ProductRepository.Delete(product);
                    var result = await _unitOfWork.SaveChangesAsync();
                    if (result == true)
                    {
                        return new ApiSuccessResult<bool>($"Delete Product [{name}] successfully!");
                    }
                }
                else
                {
                    return new ApiErrorResult<bool>($"Cannot found [{name}]");
                }
            }
            catch (Exception ex)
            {
                return new ApiErrorResult<bool>(ex.Message);
            }
            return new ApiErrorResult<bool>($"Delete Product [{name}] failed!");
        }

        public async Task<ApiResult<ProductDto>> GetByName(string name)
        {
            try
            {
                var product = await _unitOfWork.ProductRepository.GetByName(name);
                if (product != null)
                {
                    var result = _mapper.Map<ProductDto>(product);
                    return new ApiSuccessResult<ProductDto>(result);
                }
                return new ApiErrorResult<ProductDto>($"Cannot found [{name}]!");
            }
            catch (Exception ex)
            {
                return new ApiErrorResult<ProductDto>(ex.Message);
            }
        }

        public async Task<ApiResult<IList<ProductDto>>> GetListByName(string name)
        {
            try
            {
                var products = await _unitOfWork.ProductRepository.GetListByName(name);
                if (products != null && products.Count > 0)
                {
                    var result = _mapper.Map<IList<ProductDto>>(products);
                    return new ApiSuccessResult<IList<ProductDto>>(result);
                }
                return new ApiErrorResult<IList<ProductDto>>($"Cannot found [{name}]!");
            }
            catch (Exception ex)
            {
                return new ApiErrorResult<IList<ProductDto>>(ex.Message);
            }
        }

        public async Task<IList<ProductDto>> GetListByCategoryId(Guid id)
        {
            var result = await _unitOfWork.ProductRepository.GetListByCategoryId(id);
            var products = _mapper.Map<List<ProductDto>>(result);
            return products;
        }

        public async Task<ApiResult<bool>> AddRange(IList<ProductImportDto> productImports)
        {
            try
            {
                var products = _mapper.Map<IList<Product>>(productImports);
                await _unitOfWork.ProductRepository.AddRange(products);
                var result = await _unitOfWork.SaveChangesAsync();
                return new ApiSuccessResult<bool>("Import Product Success!");
            }
            catch
            {
                return new ApiErrorResult<bool>("Import Product Failed!");
            }
        }

        public async Task<ApiResult<PagedResult<ProductDto>>> GetProductsPaging(GetProductPagingRequest request)
        {
            var result = await _unitOfWork.ProductRepository.GetProductsPaging(request);
            var pagedProduct = _mapper.Map<PagedResult<ProductDto>>(result);
            return new ApiSuccessResult<PagedResult<ProductDto>>(pagedProduct);
        }

        public async Task<ApiResult<ProductDto>> GetById(Guid id)
        {
            try
            {
                var product = await _unitOfWork.ProductRepository.GetById(id);
                if (product != null)
                {
                    var result = _mapper.Map<ProductDto>(product);
                    return new ApiSuccessResult<ProductDto>(result);
                }
                return new ApiErrorResult<ProductDto>($"Cannot found!");
            }
            catch (Exception ex)
            {
                return new ApiErrorResult<ProductDto>(ex.Message);
            }
        }

        public async Task<ApiResult<ProductDto>> GetIncludeCategoryById(Guid id)
        {
            try
            {
                var product = await _unitOfWork.ProductRepository.GetIncludeCategoryById(id);
                if (product != null)
                {
                    var result = _mapper.Map<ProductDto>(product);
                    return new ApiSuccessResult<ProductDto>(result);
                }
                return new ApiErrorResult<ProductDto>($"Cannot found!");
            }
            catch (Exception ex)
            {
                return new ApiErrorResult<ProductDto>(ex.Message);
            }
        }

        public async Task<ApiResult<bool>> DeleteById(Guid id)
        {
            var product = await _unitOfWork.ProductRepository.GetById(id);
            try
            {
                if (product != null)
                {
                    _unitOfWork.ProductRepository.Delete(product);
                    var result = await _unitOfWork.SaveChangesAsync();
                    if (result == true)
                    {
                        return new ApiSuccessResult<bool>($"Delete Product [{product.Name}] successfully!");
                    }
                }
                else
                {
                    return new ApiErrorResult<bool>($"Cannot found");
                }
            }
            catch (Exception ex)
            {
                return new ApiErrorResult<bool>(ex.Message);
            }
            return new ApiErrorResult<bool>($"Delete Product [{product.Name}] failed!");
        }
    }
}
