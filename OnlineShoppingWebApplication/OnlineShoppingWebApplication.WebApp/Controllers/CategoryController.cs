﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineShoppingWebApplication.Service.Services;
using OnlineShoppingWebApplication.Service.Services.DTOs;
using OnlineShoppingWebApplication.ViewModels.Paging.Category;
using System;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.WebApp.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly INotyfService _notyfService;
        public CategoryController(ICategoryService categoryService, INotyfService notyfService)
        {
            _categoryService = categoryService;
            _notyfService = notyfService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string keyword, int pageIndex = 1, int pageSize = 3)
        {
            GetCategoryPagingRequest request = new GetCategoryPagingRequest()
            {
                Keyword = keyword,
                PageSize = pageSize,
                PageIndex = pageIndex
            };

            ViewBag.Keyword = request.Keyword;

            var categories = await _categoryService.GetCategoriesPaging(request);
            return View(categories.ResultObj);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CategoryCreateDto dto)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var result = await _categoryService.Create(dto);
            if (result.IsSuccessed)
            {
                _notyfService.Success(result.Message);
                return RedirectToAction("Index");
            }

            _notyfService.Error(result.Message);
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            var category = await _categoryService.GetById(id);
            if (category.IsSuccessed)
            {
                return View(category.ResultObj);
            }

            _notyfService.Error(category.Message);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CategoryDto dto)
        {
            if (!ModelState.IsValid)
            {
                return View(dto);
            }

            var category = await _categoryService.Edit(dto);
            if (category.IsSuccessed)
            {
                _notyfService.Success(category.Message);
                return RedirectToAction("Index");
            }

            _notyfService.Error(category.Message);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _categoryService.DeleteById(id);
            if (result.IsSuccessed)
            {
                _notyfService.Success(result.Message);
            }
            else
            {
                _notyfService.Error(result.Message);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Details(Guid id)
        {
            var category = await _categoryService.GetById(id);
            if (category.IsSuccessed)
            {
                return View(category.ResultObj);
            }

            _notyfService.Error(category.Message);
            return View();
        }
    }
}
