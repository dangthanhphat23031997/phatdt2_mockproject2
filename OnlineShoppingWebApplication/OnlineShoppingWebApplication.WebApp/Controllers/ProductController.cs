﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineShoppingWebApplication.Service.Services;
using OnlineShoppingWebApplication.Service.Services.DTOs;
using OnlineShoppingWebApplication.ViewModels.Paging.Product;
using System;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.WebApp.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly INotyfService _notyfService; 
        public ProductController(IProductService productService, ICategoryService categoryService, INotyfService notyfService)
        {
            _productService = productService;
            _categoryService = categoryService;
            _notyfService = notyfService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string keyword, int pageIndex = 1, int pageSize = 3)
        {
            ViewBag.Keyword = keyword;

            var request = new GetProductPagingRequest()
            {
                Keyword = keyword,
                PageIndex = pageIndex,
                PageSize = pageSize
            };

            var products = await _productService.GetProductsPaging(request);
            return View(products.ResultObj);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var categories = await _categoryService.GetAll();
            ViewBag.CategoryList = categories;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ProductCreateDto dto)
        {
            if (!ModelState.IsValid)
            {
                var categories = await _categoryService.GetAll();
                ViewBag.CategoryList = categories;
                return View(dto);
            }
            var result = await _productService.Create(dto);
            if (result.IsSuccessed)
            {
                _notyfService.Success(result.Message);
                return RedirectToAction("Index");
            }

            _notyfService.Error(result.Message);
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            var product = await _productService.GetById(id);
            var categories = await _categoryService.GetAll();
            ViewBag.CategoryList = categories;

            if (product.IsSuccessed)
            {
                return View(product.ResultObj);
            }

            _notyfService.Error(product.Message);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ProductDto dto)
        {
            if (!ModelState.IsValid)
            {
                var categories = await _categoryService.GetAll();
                ViewBag.CategoryList = categories;
                return View(dto);
            }

            var product = await _productService.Edit(dto);
            if (product.IsSuccessed)
            {
                _notyfService.Success(product.Message);
                return RedirectToAction("Index");
            }

            _notyfService.Error(product.Message);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _productService.DeleteById(id);
            if (result.IsSuccessed)
            {
                _notyfService.Success(result.Message);
            }
            else
            {
                _notyfService.Error(result.Message);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Details(Guid id)
        {
            var product = await _productService.GetIncludeCategoryById(id);

            if (product.IsSuccessed)
            {
                return View(product.ResultObj);
            }

            _notyfService.Error(product.Message);
            return View();
        }
    }
}
